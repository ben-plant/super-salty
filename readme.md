# A WORDPRESS THEME USING POLYMERJS

I wanted a theme that incorporated and used web components and elements as described by PolymerJS and so I decided to build this one, based on the excellent [Wordpress/Gulp Starter Kit](https://github.com/synapticism/wordpress-gulp-starter-kit).  

**This is my first effort at Open Source development so please pardon any inaccuracies or problems with it**, and I'd welcome any and all contributions/criticism. The theme itself is called Super Salty because that's how I got when I first started with it. Some of this has been copied and pasted from the README file for Wordpress/Gulp Starter Kit because their documentation is so excellent - if you get stuck getting it running, check over there as well. The bower.json file included here should grab all the Polymer stuff necessary to get this up and running for you.

### TO GET IT WORKING

* Install [npm](http://blog.npmjs.org/post/85484771375/how-to-install-npm).
* Install [Gulp](http://gulpjs.com/): `npm install gulp -g`.
* Install [Bower](http://bower.io/): `npm install bower -g`
* Download or clone this repo: `git clone https://ben-plant@bitbucket.org/ben-plant/super-salty.git`.
* You'll also need the [REST API plugin for Wordpress](https://en-gb.wordpress.org/plugins/rest-api/).
* Run `gulp`
* Run `bower install`

## LICENSE

Licensed under the [GPL 3.0](http://www.gnu.org/licenses/gpl.txt).