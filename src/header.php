<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<script src="<?php echo get_template_directory_uri(); ?>/../bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>

<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../polymer_components/story-list.html">

<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/iron-icons/iron-icons.html">
<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/iron-icons/communication-icons.html">
<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/iron-icons/maps-icons.html">

<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/paper-styles/color.html">
<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/paper-drawer-panel/paper-drawer-panel.html">
<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/paper-scroll-header-panel/paper-scroll-header-panel.html">
<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/paper-toolbar/paper-toolbar.html">
<link rel="import" href="<?php echo get_template_directory_uri(); ?>/../bower_components/paper-icon-button/paper-icon-button.html">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> class="fullbleed layout vertical">
	<paper-drawer-panel>
		<paper-scroll-header-panel drawer fixed>
			<paper-toolbar>
				<div>Menu</div>
			</paper-toolbar>
			<story-list></story-list>
		</paper-scroll-header-panel>
		<paper-scroll-header-panel main fixed>
				<paper-toolbar class="middle-tall">
					<paper-icon-button icon="menu" paper-drawer-toggle></paper-icon-button>
					<span class="top title app-name"><?php bloginfo( 'name' ); ?></span>
					<!-- <div horizontal end-justified layout> -->
					<paper-icon-button class="top" icon="search"></paper-icon-button>
					<paper-icon-button class="top" icon="communication:email"></paper-icon-button>
					<!-- </div> -->
				</paper-toolbar>
			<div id="page" class="hfeed site">
				<div id="wrap-header" class="wrap-header">
					<header id="masthead" class="site-header">
						<div class="site-branding">
							<dom-element link="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>"></dom-element>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
						</div>
						<button id="responsive-menu-toggle"><?php _e( 'Menu', 'voidx' ); ?></button>
						<nav id="site-navigation" class="site-navigation">
							<div id="responsive-menu"><?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'menu-header', 'menu_class' => 'menu-inline' ) ); ?></div>
						</nav>
					</header> 
				</div>
				<div id="wrap-main" class="wrap-main">
